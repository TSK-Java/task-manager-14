package ru.tsc.kirillov.tm.util;

import java.util.Date;
import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        return Integer.parseInt(nextLine());
    }

    static Date nextDate() {
        return DateUtil.toDate(nextLine());
    }

}
